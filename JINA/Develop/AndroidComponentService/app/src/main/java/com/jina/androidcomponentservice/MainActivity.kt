package com.jina.androidcomponentservice

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.jina.androidcomponentservice.sample_1.ServiceActivity

class MainActivity : AppCompatActivity() {

    private lateinit var btnSample1: Button
    private lateinit var btnSample2: Button

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnSample1 = findViewById(R.id.btnSample1)
        btnSample2 = findViewById(R.id.btnSample2)
        btnSample1.setOnClickListener {
            val intent = Intent(this, ServiceActivity::class.java)
            startActivity(intent)
        }

        btnSample2.setOnClickListener {

        }

    }
}