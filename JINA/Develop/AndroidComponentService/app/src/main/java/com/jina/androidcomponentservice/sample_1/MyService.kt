package com.jina.androidcomponentservice.sample_1

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.widget.Toast

class MyService : Service() {

    private val mBinder = LocalBinder()
    private var number: Int = 0

    inner class LocalBinder : Binder() {
        fun getService(): MyService {
            /*
            * NOTE : 클라이언트 코드에서 서비스의 인스턴스에 접근하기 위해 호출할 함수.
            *  -> Service 를 상속 받는 Service class 를 접근할 수 있다.
            * */
            return this@MyService
        }
    }

    /**
     * 핵심 IBinder API는 Binder.onTransact()와 일치하는 transact()입니다.
     * 이러한 메서드를 사용하면 IBinder 개체로 호출을 보내고 Binder 개체로 들어오는 호출을 각각 수신할 수 있습니다.
     * 이 트랜잭션 API는 동기적이므로 대상이 Binder.onTransact()에서 돌아올 때까지 transact() 호출이 반환되지 않습니다.
     * 이것은 로컬 프로세스에 존재하는 개체를 호출할 때 예상되는 동작이며,
     * 기본 프로세스 간 통신(IPC) 메커니즘은 프로세스 간에 이러한 동일한 의미론이 적용되도록 보장합니다.
     * */
    override fun onBind(intent: Intent?): IBinder? {
        /*
        * NOTE : Binder 를 상속받는 클래스의 인스턴스를 mBinder 라는 변수에 저장 후,
        *  클라이언트에게 반환되는 바인더 객체로 return 한다.
        * */
        return mBinder
    }

    override fun onCreate() {
        super.onCreate()
        /*
        * NOTE : 서비스가 생성될 때 호출되는 콜백메서드.
        *  -> 최초에 한번 실행 후 서비스가 종료되지 않는 이상 호출되지 않는다.
        * */
        Toast.makeText(applicationContext, "Service Created", Toast.LENGTH_SHORT).show()
        number = 0
    }

    fun getNumber(): Int {
        return number++
    }
}