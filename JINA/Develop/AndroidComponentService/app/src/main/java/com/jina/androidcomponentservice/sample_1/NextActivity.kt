package com.jina.androidcomponentservice.sample_1

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.widget.Toast
import com.jina.androidcomponentservice.databinding.ActivityNextBinding

class NextActivity : AppCompatActivity() {

    private val binding: ActivityNextBinding by lazy {
        ActivityNextBinding.inflate(layoutInflater)
    }
    private var myService = MyService()
    private var isService = false // 현재 서비스 사용 여부 확인 변수.

    private val conn = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as MyService.LocalBinder
            myService = binder.getService()
            isService = true
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            isService = false
            Toast.makeText(this@NextActivity, "서비스 연결 해제", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.btnStartService.setOnClickListener {
            val intent = Intent(this@NextActivity, MyService::class.java)
            bindService(intent, conn, Context.BIND_AUTO_CREATE)
        }

        binding.btnStopService.setOnClickListener {
            if (isService) {
                unbindService(conn)
                isService = false
            } else {
                Toast.makeText(this@NextActivity, "연결된 서비스 없음", Toast.LENGTH_SHORT).show()
            }
        }

        binding.btnCheckData.setOnClickListener {
            if (!isService) {
                Toast.makeText(this@NextActivity, "서비스 중이 아닙니다, 데이터 받을 수 없음", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }

            val number = myService.getNumber()
            Toast.makeText(this@NextActivity, "받아온 데이터 $number", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if (isService) {
            unbindService(conn)
            isService = false
        }
    }
}