package com.jina.androidcomponentservice.sample_1

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.jina.androidcomponentservice.R
import com.jina.androidcomponentservice.sample_1.MyService.LocalBinder


class ServiceActivity : AppCompatActivity() {

    lateinit var btnStartService: Button
    lateinit var btnStopService: Button
    lateinit var btnCheckData: Button
    lateinit var btnNextActivity: Button
    private var myService = MyService() // NOTE : Service class 의 인스턴스
    private var isService = false // NOTE : Service 활성 여부

    private val conn: ServiceConnection = object : ServiceConnection {
        /*
        * NOTE : bindService 를 사용하기 위해서 필요한 Callback Interface 이다.
        * */
        override fun onServiceConnected( // NOTE : 서비스가 연결될 때 호출되는 콜백메서드.
            name: ComponentName,
            service: IBinder
        ) {
            val binder = service as LocalBinder
            myService = binder.getService()
            isService = true
        }

        override fun onServiceDisconnected(name: ComponentName) { // NOTE : 서비스가 연결 해제될 때 호출되는 콜백메서드.
            isService = false
            Toast.makeText(
                applicationContext,
                "서비스 연결 해제",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)

        btnStartService = findViewById(R.id.btnStartService)
        btnStopService = findViewById(R.id.btnStopService)
        btnCheckData = findViewById(R.id.btnCheckData)
        btnNextActivity = findViewById(R.id.btnNextActivity)

        btnStartService.setOnClickListener {
            val intent = Intent(this@ServiceActivity, MyService::class.java)
            bindService(intent, conn, Context.BIND_AUTO_CREATE)
        }

        btnStopService.setOnClickListener {
            if (isService) {
                unbindService(conn)
                isService = false
            } else {
                Toast.makeText(this@ServiceActivity, "연결된 서비스 없음", Toast.LENGTH_SHORT).show()
            }
        }

        btnCheckData.setOnClickListener {
            if (!isService) {
                Toast.makeText(this@ServiceActivity, "서비스중이 아닙니다, 데이터 받을 수 없음", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }

            val number = myService.getNumber()
            Toast.makeText(this, "받아온 데이터 $number", Toast.LENGTH_SHORT).show()
        }

        btnNextActivity.setOnClickListener {
            val intent = Intent(this@ServiceActivity, NextActivity::class.java)
            startActivity(intent)
        }


    }


    override fun onDestroy() {
        super.onDestroy()
        if (isService) {
            unbindService(conn)
            isService = false
        }
    }
}