package com.jina.androidcomponents

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    // THEORY : 기본적으로 안드로이드 4대 컴퍼넌트 중 Activity 바로 생성.
    // Activity 는 말 그대로 사용자가 직접적으로 바라 볼 수 있는 UI이다.
    private lateinit var btnStart: Button
    private lateinit var btnStop: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val intent = Intent(this@MainActivity, MusicService::class.java)
        btnStart = findViewById(R.id.btnStart)
        btnStop = findViewById(R.id.btnStop)

        btnStart.setOnClickListener {
            startService(intent)
        }

        btnStop.setOnClickListener {
            stopService(intent)
        }


    }
}