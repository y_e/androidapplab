package com.jina.androidcomponents

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.util.Log

class MusicService : Service() {

    val music: MediaPlayer by lazy {
        MediaPlayer.create(this, R.raw.music)
    }

    override fun onCreate() {
        Log.d("TAG", "onCreate")
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("TAG", "onStartCommand")
        music.isLooping = true
        music.start()
        return START_REDELIVER_INTENT
        /**
         * THEORY : 음악을 종료하지 않고 앱을 종료했을 때,
         * START_NOT_STICKY : 음악이 바로 종료가 된다.
         * START_STICKY : 음악이 재실행된다.
         * START_REDELIVER_INTENT : 다시 재 실행 된다.
         */
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        Log.d("TAG", "onDestroy")
        music.stop()
        super.onDestroy()
    }

}