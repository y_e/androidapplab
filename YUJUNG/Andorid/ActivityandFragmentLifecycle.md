Activity lifeCycle
---
(사진 넣기)

1. onCreate
    - 시스템이 액티비티를 생성할 때 실행된다. 다른 콜백 메소드들과는 다르게 꼭 오버라이팅을 구현해야한다. 이 메소드에는 액티비티 전체 수명 주기 동안 딱 한 번만 동작되는 초기화 및 시작 로직을 실행할 수 있다.
2. onStart
    - onCreate 호출 후 STARTED 상태가 되면 onStart가 호출된다. 이 때 액티비티가 사용자에게 보여지고 포크라운드 태스크로 사용자와 상호작용을 할 수 있도록 준비한다. (빠른 속도로 실행되고 onResume으로 넘어간다.)
3. onResume
    - 액티비티가 RESUMED 상태에 진입하게 되면 포르가운드에 액티비티가 표시되고 앱이 사용자와 상호작용할 수 있는 상태가 된다. 
4. onPause
    - 사용자가 잠시 액티비티를 떠났을 때 호출되는 콜백 메소드로 액티비티가 포그라운드에 없을 동안 계속 실행되어서는 안되지만 언젠가 다시 시작할 작업을 일시중지하는 작업을 수행한다.
    - onPause는 잠깐 실행되어야하기 때문에 무언갈 저장하는 작업을 실행하기엔 시간이 부족할 수 있다. 데이터 저장, 네트워크 호출 ,DB 트랙잭션등은 onStop에서 수행해야한다.
    - 다시 액티비티가 재개되면 메모리상 남아있던 액티비티 인스턴스를 다시 불러와 onResume 메소드를 호출한다. 만약 액티비티가 화면에 완전히 보이지 않게 되면 onStop이 호출된다.
5. onStop
    - 액티비티가 사용자에게 더 이상 표시되지 않으면 STOPPED 상태에 진입하고 시스템에 onStop 콜백 메소드를 호출한다. 새로 시작된 액티비티가 화면 전체를 차지할 경우, 액티비티의 실행이 완료되어 종료될 시점에 호출된다. 이 때 필요하지 않은 리소스를 해제하거나 조정해야한다. 예를 들어 GPS 위치 인식 정확도를 세밀한 위치-> 대략적인 위치로 전환 (배터리 아끼기위해)
        * UI 관련 중지 작업을 할 때는 onStop일 까 onPause일 까?
            - onStop() 을 사용해야 멀티 윈도우를 사용하여 동시에 두 앱을 구동시키고 있어도 애니메이션 등 UI 구성 요소가 멈추지 않는다. 왜냐하면, 멀티 윈도우의 다른 앱으로 포커스를 조정하는 순간 onPause() 가 호출되기 때문이다. onPause() 내에 UI 중지 작업을 수행해버리면, 멀티 윈도우 상으로 액티비티는 화면에 표시되고 있는데도 UI 가 멈추어버리는 현상이 발생하게 된다. 이러한 이유로, UI 관련 마무리 작업은 onStop() 에서 수행하는 것이 낫다.
        * 액티비티가 'STOPPED' 상태에 들어가더라도 액티비티 객체는 메모리 안에 머무른다. 그런데 시스템이 더 우선순위가 높은 프로세스를 위해 메모리를 확보해야하는 경우, 이 액티비티를 메모리 상에서 죽이게 된다. 하지만 그럼에도 Bundle 에 View 객체 상태를 그대로 저장해두고, 사용자가 이 액티비티로 다시 돌아오게 되면 이를 기반으로 상태를 복원한다. (꽤나 잘 돌아간다)
    - 만약 사용자가 다시 액티비티로 돌아오게 되면 STOPPED 상태에서 RESUMED 상태로 변화하여 포그라운드 액티비티로써 태스크를 시작하며 사용자와 상호작용을 시작한다.
    - 사용자에 의해나 시스템에 의해 액티비티가 완전히 실행 종료 될 떄면 onDestroy
6. onDestroy
    - 액티비티가 완전히 소멸되기 전에 이 콜백 메소드가 호출된다. 
    - finish()가 호출되거나 사용자가 앱을 종료하여 액티비티가 종료되는경우
    - 기기 회전 등 화면 구성이 변경되어 일시적으로 액티비티를 소멸시키는 경우
        -> 이 경우는 onCreate를 다시 호출한다.
    +  onDestroy가 호출 되기 까지 해제되지 않은 리소스가 있다면 여기서 해제해주어야한다. 안그러면 Memory leak의 위험이 있다.


Fragment Lifecycle
---
1. onAttach
    - onAttach()는 프래그먼트가 FragementManager에 추가되고, 호스트 액티비티에 연결될 때 호출된다. 이 시점에 프래그먼트가 활성화되고, FragmentManager는 프래그먼트의 생명주기를 관리한다.
2. onCreate
    - FragmentManager에 추가되었을 때 CREATED가 되어 onCreate 콜백 함수를 호출한다. 
    - 중요한 점은 아직 Fragment View가 생성되지 않았기 때문에 Fragment의 View와 관련된 작업을 수행하지 않도록 해야한다.
3. onCraetView
    - Fragment가 View를 그리기 위한(Layout을 Inflate 하는 작업) 작업을 수행하는 부분이다. 따라서 반환 값도 View가 된다.
4. onViewCreated
    onCreateView를 통해 반환된 View객체는 onViewCreated의 파라미터로 전달되어 이 시점부터 Fragment view의 lifecycle이 INITIALIZED상태로 업데이트 되었기 때문에 View의 초기값을 설정해주거나 LiveData observing,RecyclerView가 pager에 사용할 adapter초기화 등을 이곳에서 수행하는 것이 적절하다.
5. onViewStateRestored
    저장해준 모든 state 값이 Fragment의 view계층구조에 복원됐을 때 호출된다. View lifecycler owner는 이 때 INITIALIZED -> CREATED 상태로 변경되었음을 알린다.
6. onStart
    - Fragment가 사용자에게 보여질 수 있을 때 호출된다. 이 시점부터는 Fragment의 child FragmentManager를 통해 FragmentTransaction을 안전하게 수행할 수 있다.
7. onResume
    - Fragment가 보이는 상태에서 모든 Animator와 Transition효과가 종료되고, 프래그먼트가 사용자와 상호작용할 수 있을 때 onResume콜백이 호출된다
    - 이 상태가 되었다는 것은 사용자가 프래그먼트와 상호작용하기에 적절한 상태라는 뜻이다.
8. onPause
    - 사용자가 Fragment를 떠나기 시작했지만 Fragment는 여전히 visible일 때 호출된다.
9. onStop
    - Fragment가 더이상 화면에 보이지 않게 되면 Fragment와 View의 Lifecycle은 CREATED상태가 되고, onStop콜백 함수가 호출되게 된다. 
    - onStop이 onSaveInstanceState함수보다 먼저 호출되어 onStop이 FragmentTransaction을 안전하게 수행할 수 있는 마지막 지점이 된다.
10. onDestroyView
    - Fragment가 화면으로부터 벗어났을 경우 FragmentView의 Lifecycler은 DESTROYED가 되고 onDestroy가 호출된다. 
    - 해당 시점에서는 가비지 컬렉터에 의해 수거될 수 있도록 Fragment View에 대한 모든 참조가 제거되어야한다.
11. onDestroy
    - Fragment가 제거되거나 FragmentManager가 destroy됐을 경우, 프래그먼트의 Lifecycle은 DESTROYED상태가 되고, onDestroy콜백 함수가 호출된다. 해당 지점은 lifecycle의 끝을 알린다.