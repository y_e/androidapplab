BroadCastReceiver
---
- 단말기에서 발생하는 다양한 이벤트, 정보를 받고 반응하는 컴포넌트
- 안드로이드 앱은 안드로이드 시스템 및 기타 안드로이드 앱에서 게시-구독 디자인 패턴과 유사한 브로드캐스트 메세지를 받거나 보낼 수 있다. 이러한 관심 있는 이벤트가 발생할 때 브로드캐스트가 전송 된다
- 예를 들어 안드로이드 시스템은 시스템 부팅 or. 기기 충전 시작 or 배터리 부족 과 같은 다양한 시스템 이벤트가 발생할 때 브로드캐스트 전송

앱은 특정 브로드 캐스트를 수신하도록 등록 할 수 있고, 브로드 캐스트가 전송되면 시스템은 특정 유형의 브로드 캐스트를 수신하도록 신청한 앱에 브로드 캐스트를 자동 라우팅한다. 


### 종류는 무엇이 있을까

1. 동적 리시버
    - 클래스 파일에서 리시버를 등록하고 해제할 수 있는 형태이기 때문에 시스템이나 앱에 부하를 줄일 수 있지만 해제를 적절히 해주지 않는다면 메모리 릭이 발생할 수 있다.

2. 정적 리시버
    - 사용자가 직접 AndroidManifest.xml파일에 등록하여 리시버를 구현하는 형태인데, 이 형태는 한 번 등록 하면 해제할 수 없는 방식이다.

- Manifest or  registerReceiver() 를 통한 리시버 등록 -> 등록한 리시버에서 인텐트 필터 -> onReceiver()메소드 정의 ->(동적 리시버의 경우)unregisterReciver()를 통한 리시버 해제

리시버에는 너무 많은 작업, 시간이 오래걸리는 작업을 하면 안된다. 처리 지연시간이 길어진 경우 ANR이 발생하기 때문에 리시버에는 간단한 일을 처리하도록 하고, 스레드를 별도로 생성해서 처리하도록 해야한다.