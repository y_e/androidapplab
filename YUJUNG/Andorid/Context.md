## Context

1. 프로그래밍에서의 context는 상황, 맥락, 문맥을 의미하며 개발자가 어떠한 작업을 완료하는 데 필요한 모든 관련 정보를 말한다.

2. 안드로이드에서는 Application 환경에 대한 글로벌 정보를 가지는 인터페이스라고 한다.
     - interface? 서로 다른 두 개의 시스템 사이에서 정보나 신호를 주고 받는 경우의 접점이나 경계면으로 사용자 기기를 쉽게 동작시키는데 도움을 주는 시스템을 의미한다.
3. context를 잘 못 사용하게되면 앱이 비정상적으로 종료되거나 메모리 누수가 발생한다.
4. context는 App의 현재 상태를 나타낸다.
5. Activity와 App의 정보를 얻기 위해 사용할 수 있다.
6. 리소스, DB, shared preference등에 접근하기 위해 사용한다.
7. Activity와 App class에는 Context class를 확장한 Class이다.

### Toast를 띄울 때 context가 사용된다.
---

Toast는 일반적인 View와 달리 특정 Activity와 연결되어 있는 window에 속하지 않고 독립적인 윈도우를 생성하기 때문이다. 

+ Toast메세지가 켜져있는 상태에서 빠르게 종료를 한다면 화면에 메세지가 남아있는 현상을 볼 수 있다. 
+ 응용 프로그램 환경에 대한 글로벌 정보에 대한 인터페이스입니다. 이것은 안드로이드 시스템에 의해 구현되는 추상 클래스이다. 애플리케이션별 리소스 및 클래스에 대한 액세스와 함께 시작 활동, 브로드캐스트 및 수신 의도 등과 같은 애플리케이션 수준 운영에 대한 업콜을 허용한다고 한다. (즉 context가 없으면 Acitivy시작, 브로드캐스트 발생, 서비스 시작을 못한다. 리소스에 접근할 때도 Context를 통해서만 가능하다. Context는 여러 컴포넌트의 상위 클래스이다.)

### Context의 종류
---

1. Application context
    -  싱글톤 인스턴스이며, Activity에서 getApplicationContext를 통해 접근할 수 있다.
    - Application lifecycle에 관련되어있으며, 현재 Activity가 종료된 이후에도 Context가 필요한 작업 또는 Activity 스코프를 벗어난 Context가 필요한 작업에서 사용하면 좋다.
2. Activity Context
    - Activity내에서 유효한 context로 Activity lifecycle에 관련되어있으며 Activity가 소멸할 때 context도 같이 소면된다.
    - 예 : Activity lifecycle과 같은 object를 생성해야할 때 사용할 수 있다.
