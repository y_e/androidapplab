각각의 컴포넌트간의 통신을 맡고 있는 것이 인텐트이다.

intent 통신 방법은 두가지이다.
1. 명시적 intent
가장 많이 볼 수 있는 방법으로 앱의 화면 전환을 하는 방법이다. 하나의 액티비티에서 다른 액티비티로의 화면 전환시 사용하는 것

    현재 TestA activity에서 다른 TestB액티비티를 호출할 때 사용하는 예제이다. 
    ```
    Intent intent = new Intent(TestA.this, TestB.class);
    startActivity(intent);
    ```

- 인텐트는 액티비티 호출 시 데이터를 전달 또는 데이터를 리턴받을 수 있다.
데이터 전달하는 예

    1-1. TestA - 데이터를 보내고, TestB - 데이터를 받는 예제

        Intent intent = new Intent(TestA.this, TestB.class);
        intent.putExtra("key", "데이터가 전달 되었습니다.");
        startActivity(intent);

        Intent getIntent = getIntent();
        String value = getIntent.getStringExtra("key");

    2-2. TestA에서 TestB에 데이터를 전달하고 TestB종료 시 리턴값을 받는 경우
    (TestA - startActivityForResult() 메소드로 TestB를 호출하고 onActivityResult()로 리턴값을 받는 경우)

        Intent intent = new Intent(TestA.this, TestB.class);
        startActivityForResult(intent,100);

        @override
        protected void onActivityResult(int requestCode, int resultCode, Intent data){
	        switch(requestCode){
		        case 100:
			        if(data.getExtras() != null){
				        String str = data.getStringExtra("resultKey");
				        result01.setText("startActivityForResult() 리턴 값 받기 : "+str);
			    }
			break;
	    }
	        super.onActivityResult(requestCode, resultCore, data);
        }

2. 암시적 intent
---
Action에 따라 해당하는 적합한 애플리케이션의 클래스를 호출한다. 이 떄 하나가 아닌 여러개가 호출 될 수 있다.
예는 웹브라우저 호출, 이메일 전송, 전화 앱으로의 통화 등이 해당한다.

웹브라우저 인텐트 호출

    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://m.naver.com"));
    startActivity(intent);
    ...

* intentFilter
암시적 인텐트를 통해 사용자로 하여금 어느 앱을 사용할지 선택하도록 하고자 할 때 필요한 것. 예를 들어 사용자가 다른 사람들과 공유했으면 하는 콘텐츠를 가지고 있는 경우, ACTION_SEND라는 Action 있는 인텐트를 생성한 다음 공유할 콘텐츠를 지정하는 Extra를 추가하면 된다.
해당 인텐트로 startActivity()를 호출하면 사용자가 어느 앱을 통해 콘텐츠를 공유할지 선택할 수 있습니다.
예를 들어, A 앱의 액티비티에서 B 앱의 액티비티를 실행한다고 가정하겠습니다. 두 앱은 서로 외부 앱이므로 연동 시 다른 앱의 컴포넌트 클래스명을 알 수도 없고, 코드에서 클래스명을 명시할 수도 없습니다. 그래서 클래스명 대신 Intent Filter 정보를 담아 실행하면 시스템에서 Intent Filter 정보를 해석해서 컴포넌트를 실행하는 것이 암시적 인텐트입니다.

* pendingIntent
 Pending - 보류, 임박
 가지고 있는 Intent를 당장 수행하진 않고, 특정 시점에 수행하도록 하는 특징을 가지고 있다. 여기서 말하는 특정 시점은 보통 앱이 구동 되고 있지 않을 때이다.
 예를 들어 대용량 파일을 다운로드 하는 동작을 구현 -> 사용자들은 기다리기 보다는 다른 앱을 사용하고 있기 때문에 다운로드가 다 되었을 때를 알리는 것 또는 notification
 
 * 왜 일반 Intent로 하면 안될까? 다른 앱에서 내가 정의한 Intent를 실행한다는 뜻이 되어 이는 불가능하기 때문이다.

 * PendingIntent를 사용하면 특정 시점에 Intent를 수행하는 것이 보장되어있기 때문에 정상적으로 실행된다. 이것은 어떻게 보장이 되는걸까?

    -> .Intent가 수행되는 시점이 보통 "앱이 구동되고 있지 않을 때"이므로, 다른 프로세스(앱)에게 권한을 허가하여 Intent를 마치 본인 앱에서 실행되는 것 처럼 사용하는 것이다. 때문에 특정 시점에 Intent가 무조건 수행될 수 있도록 보장되는 것이다.

* 플래그

    FLAG_CANCEL_CURRENT
        → 이전에 생성한 PendingIntent 취소 후 새로 생성

    FLAG_NO_CREATE
        → 이미 생성된 PendingIntent 가 있다면 재사용 (없으면 Null 리턴)

    FLAG_ONE_SHOT
        → 해당 PendingIntent 를 일회성으로 사용

    FLAG_UPDATE_CURRENT
        → 이미 생성된 PendingIntent 가 있다면, Extra Data 만 갈아끼움 (업데이트)