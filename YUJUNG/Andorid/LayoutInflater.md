## LayoutInflater

- xml에 미리 정의해 둔 틀을 실제 메모리에 올리는 역할을 한다. (inflater : 부풀리다)

- xml에 정의된 resource를 view객체로 반환해주는 역할을 한다.
    - 예를 들면 onCreate 의 setContentView(R.layout.test)가 inflater이다.
- LayoutInflater 는 레이아웃 XML 파일을 통해, View 객체를 실체화해주는 역할을 한다. 즉, 화면을 구성하는 XML 리소스를 View 객체로 만들어 반환해주는 역할을 한다. 
### 생성 방법
---

1. getSystemService
    - LayoutInfalter는 시스템 서비스 객체로 제공되기 때문에 getSystemService를 참조해서 참조할 수 있다.
    - getSystemService(Context.LAYOUT_INFLATER_SERVICE)

2. 뷰 객체가 있으면 그 뷰 객체에 인플래이션 한 결과를 설정하게된다. 
    - LayoutInflater를 사용하기 위한 조건 
        - 객체화 하고자 하는 xml이 있어야한다. 
        - inflater를 통해 LayoutInflater를 사용할 준비를 한다.
        - inflate를 통해 frameLayout라는 레이아웃에 작성했던 xml의 메모리객체가 삽입되게 된다.
₩₩₩
FrameLayout frameLayout = (FrameLayout)FindViewById(R.id.test);
LayoutInflater inflater = (LayoutInflater)getSystemService(Content.LAYOUT_INFLATER_SERVICE);
inflater.inflate(R.layout.sub1, frameLayout, true);
₩₩₩

#### 추가 참고 사이트
---
https://velog.io/@haero_kim/Android-LayoutInflater-알아보기