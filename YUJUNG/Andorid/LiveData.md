## LiveData

1. LiveData는 LifeCycle내에서 관찰 할 수 있는 데이터 홀더 클래스이다.
2. Observer, LifecycleOwner와 쌍으로 추가 할 수 있다. Observer에는 래핑 된 데이터의 수정에 대해 알림을 받는다.
    - LifecycleOwner상태가 Lifecycle.State.STARTED or Lifecycle.State.RESUMED 일 때 받을 수 있다.
3. observeForever는 항상 활성 상태로 간주하여 항상 알림을 받을 수 있으며 수동으로 removeObserver로 옵저버를 제거할 수 있다.
4. Livecycle에 추가 된 관찰자는 Lifecycle.State.DESTROYED 상태로 이동되면 옵저버가 즉시 구독이 취소되므로 메모리 누수에 대해 걱정할 필요가 없다는 장점을 가지고 있다.
5. LiveData는 ViewModel의 데이터 필드를 보유하도록 설계되어있다.

## 장점

1. UI가 데이터 상태와 일치하는지 확인할 수 있다.
    - 옵저버 패턴이기 때문에 LiveData observer는 기본 데이터가 변경될 때 구독자에게 알려 구독자는 알림을 받으면 해당 객체의 YU를 업데이트 할 수 있다.
2. 메모리 누수가 없다.
    - LifecycleOwner의 상태가 DESTORYED가 되면 자동으로 구독 취소된다.
3. UI의 상태가 활성상태가 아닐 때 이벤트를 수신하지 않는다.
4. 화면 회전 시 구성변겅
    - 디바이스가 회전 시 데이터들이 다시 생성되면 최신 데이터를 즉시 수신한다.