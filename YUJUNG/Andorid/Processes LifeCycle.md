## Android processes 관리 기본 원리

1. 안드로이드 어플리케이션은 리눅스 프로세스 상에서 동작한다.
2. 한 앱 컴포넌트가 시작하고 다른 앱의 컴포넌트가 실행중이지 않으면 안드로이드 시스템은 하나의 스레드 실행과 함께 새로운 리눅스 프로세스를 시작한다.
3. 컴포넌트를 실행 시작 시도 중 이미 그 앱의 다른 컴포넌트의 프로세스가 존재한다면 
    - 디폴트 : 같은 프로세스, 같은 스레드에서 시작한다.
    - 하나의 앱에 다른 각 컴포넌트를 다른 프로세스, 다른 스레드를 만들어 실행할 수 있다. 
        - 방법 : 매니페스트에 <activity>,<service>,<receiver>,<provider> 엔트리에 넣고, android:process 속성값을 주어 각 컴포넌트가 어느 프로세스에서 실행할 지 명시
4. 프로세스는 앱의 코드가 실행되어야할 때 만들어지고, 더이상 필요하지 않거나 시스템이 다른 앱을 위해 메모리를 재 확보해야할 때까지 실행된다.
5. 앱 프로세스의 생명주기는 앱 자체가 아니라 앱의 컴포넌트들의 조합을 통해 시스테에 의해서 결정된다. (그 컴포넌트들이 얼마나 유저에게 중요한지와 얼만큼의 메모리가 사용가능한지)
6. 그래서 각 구성요소들 (액티비티, 서비스, 브로드캐스트 리시버)이 어떻게 앱 프로세스 생명주기에 영향을 미치는지 알아야한다.
7. 메모리가 부족할 때, 시스템은 프로세스를 죽여야하는데, 그 순서는 실행되고 있는 컴포넌트와 컴포넌트 상태로 결정된다. 이것을 기반으로 중요한 순서를 매긴 뒤 중요하지 않은 플세스 부터 죽인다.
8. 프로세스의 우선순위는 종속성에 따라 증가할 수도 있다. 예를 들어 A가 B프로세스에 있는 컴포넌트에 종속되어 있다면 B가 우선순위가 더 높을 것

### Processes 5가지 타입
---

1. Foreground process
    - 현재 유저가 하고 있는 것을 위해 필요한 프로세스. 
    - 현재 유저가 상호작용하고 있는 화면 최상단 Activity가 실행되고 있을 때
    - BroadcastReceiver.onReceive()메서드가 실행 중 일 때
    - Service가 가진 callback인 onCreate, onStart or onDestroy중 하나로 코드가 실행되고 있을 때
    - 메모리가 부족할 때 최후의 수단으로 죽임
2. Visible process
    - 유저가 현재 인지하고 있는 것을 실행하는 프로세스
    - Activity onpause()부터 -> 스크린에는 보이지만 포그라운드가 아닐 때
    - Service.startForeground() -> 서비스가 포그라운드 서비스로서 실행될 때
    - 시스템은 foreground process를 실행 시키기에 메로리가 부족하지 않는 한 visible process를 유지시킨다.
3. Service process
    - startService()함수를 통해서 시작된 service를 가지고 있는 process
    - 사용자에게 보이지는 않지만, 사용자가 관심있어 하는 작업을 처리한다.
    - 시스템은 foreground와 visible process를 실행 시키기에 메모리가 부족하지 않는 한 service process를 유지시킨다.
4. Background process
    - 사용자에게 보이지 않는 activity를 가지고 있는 process(onStop()이 호출된 상태)
    - 사용자 경험에 직접적인 영향을 주지는 못한다.
    - 시스템은 앞의 3가지 타입을 실행시키기 위해서 언제든지 background process를 죽일 수 있다.
    - 죽지 않은 Background process들은 LRU list로 관리되어 가장 최근에 사용한 process가 가장 마지막에 죽는다
5. Empty process
    - 어떤 active application component를 가지고 있지 않은 process이다.
    - Activity Compenet를 갖고 있지 않는 Process. 살아 있는 이유는 다음에 재실행될 떄, 시작 시간을 다운시키기 위해서 시스템이 프로세스를 캐싱하고 있기 떄문이다.