## View Group

하위에 여러개의 viewGroup, view를 포함하고 있고, 각 객체들의 위치를 결정한다.
각 객체는 다양한 속성을 가질 수 있으며 속성 값에 따라 UI가 변경된다.
viewgroup은 view를 상속한다.

예) LinearLayout, FrameLayout

##view

button, EditText등 어떤기능을 하는 Component이다. View는 자신이 화면 어디에 배치되어야 하는지에 대한 정보가 없다. View만으로 화면에 나타날 수 없다. 